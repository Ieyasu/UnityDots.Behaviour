# UnityDots Behaviour

UnityDots Behaviour contains steering behaviours for the new Unity DOTS stack.

## Features

* Fast parallelized steering behaviours using Unity Burst and jobs
* Reusable movement and rotation systems
* Wander steering behaviour
* Seek steering behaviour
* Flee steering behaviour
* Pursue steering behaviour
* Evade steering behaviour

## Getting started

To include the package in your Unity project, add the following git packages using the Package Manager UI:

```
https://gitlab.com/Ieyasu/UnityDots.Splines.git?path=/src"
https://gitlab.com/Ieyasu/UnityDots.Behaviour.git?path=/src"
```

The following snippet shows how to create two entities; one with a wander steering behaviour that randomly wanders around and one with a seek steering behaviour that tries to follow the wandering agent (note that to see the agents, you must also add components required by Unity's HybridRenderer and a suitable mesh):

```cs
void CreateEntities(EntityManager entityManager)
{
    var wanderer = CreateWanderer(entityManager);
    var seeker = CreateSeeker(entityManager, wanderer);
}

Entity CreateWanderer(EntityManager entityManager)
{
    var entity = entityManager.CreateEntity();

    // The transform of the entity
    entityManager.AddComponentData(entity, new Translation { Value = float3.zero });
    entityManager.AddComponentData(entity, new Rotation { Value = quaternion.identity });
    entityManager.AddComponentData(entity, new Scale { Value = 1 });
    entityManager.AddComponentData(entity, new LocalToWorld());

    // The movement of the entity
    entityManager.AddComponentData(entity, new AccelerationLimit { MaxValue = 6 });
    entityManager.AddComponentData(entity, new Acceleration());
    entityManager.AddComponentData(entity, new VelocityLimit { MaxValue = 12 });
    entityManager.AddComponentData(entity, new Velocity());

    // The steering behaviour of the entity
    entityManager.AddComponentData(entity, new Wander
    {
        Weight = 1,
        Strength = 2.0f,
        Frequency = 0.3f
    });

    // Aligns the rotation of the agent to the velocity
    entityManager.AddComponentData(entity, new RotationAlignTag());

    return entity;
}

Entity CreateSeeker(EntityManager entityManager, Entity wanderer)
{
    var entity = entityManager.CreateEntity();

    // The transform of the entity
    entityManager.AddComponentData(entity, new Translation { Value = float3.zero });
    entityManager.AddComponentData(entity, new Rotation { Value = quaternion.identity });
    entityManager.AddComponentData(entity, new Scale { Value = 0.5f });
    entityManager.AddComponentData(entity, new LocalToWorld());

    // The movement of the entity
    entityManager.AddComponentData(entity, new AccelerationLimit { MaxValue = 3 });
    entityManager.AddComponentData(entity, new Acceleration());
    entityManager.AddComponentData(entity, new VelocityLimit { MaxValue = 10 });
    entityManager.AddComponentData(entity, new Velocity());

    // The steering behaviour of the entity
    entityManager.AddComponentData(entity, new Seek { Weight = 1 });

    // Aligns the rotation of the agent to the velocity
    entityManager.AddComponentData(entity, new RotationAlignTag());

    // Set the target to seek
    var targets = entityManager.AddBuffer<TargetEntity>(entity);
    targets.ResizeUninitialized(1);
    targets[0] = wanderer;

    return entity;
}
```
