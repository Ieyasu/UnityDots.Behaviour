using Unity.Entities;
using Unity.Transforms;

namespace UnityDots.Behaviour
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateBefore(typeof(TransformSystemGroup))]
    public sealed class RotationSystemGroup : ComponentSystemGroup
    {
    }
}
