using Unity.Entities;
using Unity.Transforms;

namespace UnityDots.Behaviour
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateAfter(typeof(RotationSystemGroup))]
    [UpdateBefore(typeof(TransformSystemGroup))]
    public sealed class MovementSystemGroup : ComponentSystemGroup
    {
    }
}
