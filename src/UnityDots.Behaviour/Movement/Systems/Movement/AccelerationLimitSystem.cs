using Unity.Entities;
using Unity.Transforms;
using static Unity.Mathematics.math;

namespace UnityDots.Behaviour
{
    [UpdateInGroup(typeof(MovementSystemGroup))]
    public sealed class AccelerationLimitSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .ForEach((ref Acceleration acc, in AccelerationLimit limit) =>
                {
                    var accMin = max(limit.MinValue, 0);
                    var accMax = max(accMin, limit.MaxValue);
                    var accLength = clamp(length(acc.Value), accMin, accMax);
                    acc.Value = accLength * normalizesafe(acc.Value);
                })
                .ScheduleParallel();
        }
    }
}
