using Unity.Entities;
using Unity.Transforms;
using static Unity.Mathematics.math;

namespace UnityDots.Behaviour
{
    [UpdateInGroup(typeof(MovementSystemGroup))]
    [UpdateAfter(typeof(AccelerationSystem))]
    public sealed class VelocityLimitSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .ForEach((ref Velocity vel, in VelocityLimit limit) =>
                {
                    var velMin = max(limit.MinValue, 0);
                    var velMax = max(velMin, limit.MaxValue);
                    var speed = clamp(length(vel.Value), velMin, velMax);
                    vel.Value = speed * normalizesafe(vel.Value);
                })
                .ScheduleParallel();
        }
    }
}
