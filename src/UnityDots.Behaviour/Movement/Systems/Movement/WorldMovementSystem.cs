using Unity.Entities;
using Unity.Transforms;
using static Unity.Mathematics.math;

namespace UnityDots.Behaviour
{
    [UpdateInGroup(typeof(MovementSystemGroup))]
    [UpdateAfter(typeof(VelocityLimitSystem))]
    public sealed class WorldMovementSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var dt = Time.DeltaTime;
            Entities
                .WithNone<LocalMovementTag>()
                .ForEach((ref Translation trans, in Velocity vel) => trans.Value += vel.Value * dt)
                .ScheduleParallel();
        }
    }
}
