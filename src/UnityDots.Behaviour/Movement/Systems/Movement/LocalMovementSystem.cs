using Unity.Entities;
using Unity.Transforms;
using static Unity.Mathematics.math;

namespace UnityDots.Behaviour
{
    [UpdateInGroup(typeof(MovementSystemGroup))]
    [UpdateAfter(typeof(VelocityLimitSystem))]
    public sealed class LocalMovementSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var dt = Time.DeltaTime;
            Entities
                .WithAll<LocalMovementTag>()
                .ForEach((ref Translation trans, in Velocity vel, in Rotation rot) =>
                {
                    var step = vel.Value * dt;
                    trans.Value += mul(rot.Value, step);
                })
                .ScheduleParallel();
        }
    }
}
