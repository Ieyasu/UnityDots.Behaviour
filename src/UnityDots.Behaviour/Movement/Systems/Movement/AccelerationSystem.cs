using Unity.Entities;
using Unity.Transforms;
using static Unity.Mathematics.math;

namespace UnityDots.Behaviour
{
    [UpdateInGroup(typeof(MovementSystemGroup))]
    [UpdateAfter(typeof(AccelerationLimitSystem))]
    public sealed class AccelerationSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var dt = Time.DeltaTime;
            Entities
                .ForEach((ref Velocity vel, in Acceleration acc) => vel.Value += acc.Value * dt)
                .ScheduleParallel();
        }
    }
}
