using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;

namespace UnityDots.Behaviour
{
    [UpdateInGroup(typeof(RotationSystemGroup))]
    [UpdateAfter(typeof(AngularAccelerationSystem))]
    public sealed class AngularVelocityLimitSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .ForEach((ref AngularVelocity vel, in AngularVelocityLimit limit) =>
                {
                    var velMin = max(limit.MinValue, 0);
                    var velMax = max(velMin, limit.MaxValue);
                    vel.Magnitude = clamp(vel.Magnitude, velMin, velMax);
                })
                .ScheduleParallel();
        }
    }
}
