using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;

namespace UnityDots.Behaviour
{
    [UpdateInGroup(typeof(RotationSystemGroup))]
    [UpdateAfter(typeof(AngularAccelerationLimitSystem))]
    public sealed class AngularAccelerationSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var dt = Time.DeltaTime;
            Entities
                .ForEach((ref AngularVelocity vel, in AngularAcceleration acc) =>
                {
                    var velVec = vel.Axis * vel.Magnitude;
                    var accVec = acc.Axis * acc.Magnitude;
                    velVec += accVec * dt;

                    var len = length(velVec);
                    if (len < FLT_MIN_NORMAL)
                    {
                        vel.Magnitude = 0;
                    }
                    else
                    {
                        vel.Magnitude = len;
                        vel.Axis = velVec / len;
                    }
                })
                .ScheduleParallel();
        }
    }
}
