using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using Unity.Mathematics;

namespace UnityDots.Behaviour
{
    [UpdateInGroup(typeof(RotationSystemGroup))]
    public sealed class RotationAlignSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var dt = Time.DeltaTime;
            Entities
                .WithAll<RotationAlignTag>()
                .WithNone<AngularVelocity>()
                .ForEach((ref Rotation rot, in Velocity vel, in LocalToWorld trans) =>
                {
                    rot.Value = quaternion.LookRotationSafe(vel.Value, trans.Up);
                })
                .ScheduleParallel();
        }
    }
}
