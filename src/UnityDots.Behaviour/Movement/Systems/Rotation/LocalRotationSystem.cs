using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;

namespace UnityDots.Behaviour
{
    [UpdateInGroup(typeof(RotationSystemGroup))]
    [UpdateAfter(typeof(AngularVelocityLimitSystem))]
    public sealed class LocalRotationSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var dt = Time.DeltaTime;

            Entities
                .WithAll<LocalRotationTag>()
                .ForEach((ref Rotation rot, in AngularVelocity vel) =>
                {
                    var rotDelta = Unity.Mathematics.quaternion.AxisAngle(vel.Axis, vel.Magnitude * dt);
                    rot.Value = mul(rot.Value, rotDelta);
                })
                .ScheduleParallel();
        }
    }
}
