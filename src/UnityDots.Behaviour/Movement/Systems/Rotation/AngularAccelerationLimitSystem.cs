using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;

namespace UnityDots.Behaviour
{
    [UpdateInGroup(typeof(RotationSystemGroup))]
    public sealed class AngularAccelerationLimitSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .ForEach((ref AngularAcceleration acc, in AngularAccelerationLimit limit) =>
                {
                    var accMin = max(limit.MinValue, 0);
                    var accMax = max(accMin, limit.MaxValue);
                    acc.Magnitude = clamp(acc.Magnitude, accMin, accMax);
                })
                .ScheduleParallel();
        }
    }
}
