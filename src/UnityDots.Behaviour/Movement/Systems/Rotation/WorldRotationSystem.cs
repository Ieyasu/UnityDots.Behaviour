using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;

namespace UnityDots.Behaviour
{
    [UpdateInGroup(typeof(RotationSystemGroup))]
    [UpdateAfter(typeof(AngularVelocityLimitSystem))]
    public sealed class WorldRotationSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var dt = Time.DeltaTime;
            Entities
                .WithNone<LocalRotationTag>()
                .ForEach((ref Rotation rot, in AngularVelocity vel) =>
                {
                    var rotDelta = Unity.Mathematics.quaternion.AxisAngle(vel.Axis, vel.Magnitude * dt);
                    rot.Value = mul(rotDelta, rot.Value);
                })
                .ScheduleParallel();
        }
    }
}
