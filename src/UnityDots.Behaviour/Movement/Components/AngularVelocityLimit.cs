using Unity.Entities;

namespace UnityDots.Behaviour
{
    public struct AngularVelocityLimit : IComponentData
    {
        public float MinValue;
        public float MaxValue;
    }
}
