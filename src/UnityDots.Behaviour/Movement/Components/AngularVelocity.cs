using Unity.Entities;
using Unity.Mathematics;

namespace UnityDots.Behaviour
{
    public struct AngularVelocity : IComponentData
    {
        public float3 Axis;
        public float Magnitude;
    }
}
