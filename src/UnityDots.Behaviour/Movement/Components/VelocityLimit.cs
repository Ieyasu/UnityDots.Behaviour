using Unity.Entities;

namespace UnityDots.Behaviour
{
    public struct VelocityLimit : IComponentData
    {
        public float MinValue;
        public float MaxValue;
    }
}
