using Unity.Entities;
using Unity.Mathematics;

namespace UnityDots.Behaviour
{
    public struct Velocity : IComponentData
    {
        public float3 Value;
    }
}
