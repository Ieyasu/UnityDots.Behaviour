using Unity.Entities;

namespace UnityDots.Behaviour
{
    public struct AngularAccelerationLimit : IComponentData
    {
        public float MinValue;
        public float MaxValue;
    }
}
