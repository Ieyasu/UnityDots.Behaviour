using Unity.Entities;

namespace UnityDots.Behaviour
{
    public struct AccelerationLimit : IComponentData
    {
        public float MinValue;
        public float MaxValue;
    }
}
