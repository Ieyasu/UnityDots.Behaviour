using Unity.Entities;
using Unity.Mathematics;

namespace UnityDots.Behaviour
{
    public struct Acceleration : IComponentData
    {
        public float3 Value;
    }
}
