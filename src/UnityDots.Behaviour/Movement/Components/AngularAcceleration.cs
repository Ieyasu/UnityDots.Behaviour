using Unity.Entities;
using Unity.Mathematics;

namespace UnityDots.Behaviour
{
    public struct AngularAcceleration : IComponentData
    {
        public float3 Axis;
        public float Magnitude;
    }
}
