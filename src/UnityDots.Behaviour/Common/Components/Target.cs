using Unity.Entities;
using Unity.Mathematics;

namespace UnityDots.Behaviour
{
    [InternalBufferCapacity(1)]
    public struct Target : IBufferElementData
    {
        public float3 Value;

        public static implicit operator float3(Target e)
        {
            return e.Value;
        }

        public static implicit operator Target(float3 e)
        {
            return new Target { Value = e };
        }
    }
}
