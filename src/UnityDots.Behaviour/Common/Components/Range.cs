using Unity.Entities;
using Unity.Mathematics;

namespace UnityDots.Behaviour
{
    public struct Range : IComponentData
    {
        public float Value;
    }
}
