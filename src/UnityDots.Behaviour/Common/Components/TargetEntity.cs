using Unity.Entities;

namespace UnityDots.Behaviour
{
    [InternalBufferCapacity(1)]
    public struct TargetEntity : IBufferElementData
    {
        public Entity Value;

        public static implicit operator Entity(TargetEntity e)
        {
            return e.Value;
        }

        public static implicit operator TargetEntity(Entity e)
        {
            return new TargetEntity { Value = e };
        }
    }
}
