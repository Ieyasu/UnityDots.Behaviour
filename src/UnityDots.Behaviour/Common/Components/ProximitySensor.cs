using Unity.Entities;

namespace UnityDots.Behaviour
{
    public enum ProximitySensorMode
    {
        Closest,
        All
    }

    public struct ProximitySensor : IComponentData
    {
        public ProximitySensorMode Mode;
    }
}
