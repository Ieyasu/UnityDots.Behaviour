
using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Collections;
using Unity.Transforms;

namespace UnityDots.Behaviour
{
    [UpdateBefore(typeof(MovementSystemGroup))]
    public sealed class TargetEntitySyncSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .ForEach((ref DynamicBuffer<Target> targets, in DynamicBuffer<TargetEntity> targetEntities) =>
                {
                    targets.ResizeUninitialized(targetEntities.Length);

                    for (var i = 0; i < targets.Length; ++i)
                    {
                        var entity = targetEntities[i].Value;
                        targets[i] = GetComponent<Translation>(entity).Value;
                    }
                })
                .ScheduleParallel();
        }
    }
}
