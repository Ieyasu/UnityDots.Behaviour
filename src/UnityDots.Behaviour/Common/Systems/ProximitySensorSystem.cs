using Unity.Entities;
using Unity.Jobs;
using Unity.Burst;
using Unity.Collections;
using Unity.Physics;
using Unity.Transforms;

namespace UnityDots.Behaviour
{
    public sealed class ProximitySensorSystem : JobComponentSystem
    {
        private NativeMultiHashMap<Entity, DistanceHit> _allHits;
        private NativeHashMap<Entity, DistanceHit> _closestHits;

        private EntityQuery _colliderQuery;
        private EntityQuery _pointQuery;

        [BurstCompile]
        private struct ColliderJob : IJobChunk
        {
            [ReadOnly] public PhysicsWorld World;
            [ReadOnly] public EntityTypeHandle EntityType;
            [ReadOnly] public ComponentTypeHandle<ProximitySensor> SensorType;
            [ReadOnly] public ComponentTypeHandle<Range> RangeType;
            // [ReadOnly] public ArchetypeChunkComponentType<LayerMask> LayerMaskType;
            [ReadOnly] public ComponentTypeHandle<Translation> TranslationType;

            [WriteOnly] public NativeArray<DistanceHit> ClosestHits;
            [WriteOnly] public NativeMultiHashMap<Entity, DistanceHit>.ParallelWriter AnyHits;

            public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
            {
            }
        }

        [BurstCompile]
        private struct PointJob : IJobChunk
        {
            [ReadOnly] public PhysicsWorld World;
            [ReadOnly] public EntityTypeHandle EntityType;
            [ReadOnly] public ComponentTypeHandle<ProximitySensor> SensorType;
            [ReadOnly] public ComponentTypeHandle<Range> RangeType;
            // [ReadOnly] public ArchetypeChunkComponentType<LayerMask> LayerMaskType;
            [ReadOnly] public ComponentTypeHandle<Translation> TranslationType;

            [WriteOnly] public NativeArray<DistanceHit> ClosestHits;
            [WriteOnly] public NativeMultiHashMap<Entity, DistanceHit>.ParallelWriter AnyHits;

            public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
            {
                var entities = chunk.GetNativeArray(EntityType);
                var sensors = chunk.GetNativeArray(SensorType);
                var translations = chunk.GetNativeArray(TranslationType);
                var ranges = chunk.GetNativeArray(RangeType);
                // var layerMasks = chunk.GetNativeArray(LayerMaskType);

                for (var i = 0; i < chunk.Count; ++i)
                {
                    var input = new PointDistanceInput
                    {
                        Position = translations[i].Value,
                        MaxDistance = ranges[i].Value,
                    };

                    switch (sensors[i].Mode)
                    {
                        case ProximitySensorMode.All:
                            CalculateAllHits(entities[i], input);
                            break;
                        case ProximitySensorMode.Closest:
                            throw new System.NotImplementedException();
                        default:
                            throw new System.ComponentModel.InvalidEnumArgumentException();
                    }
                }
            }

            private void CalculateAllHits(Entity entity, PointDistanceInput input)
            {
                var distanceHits = new NativeList<DistanceHit>(Allocator.Temp);
                if (World.CalculateDistance(input, ref distanceHits))
                {
                    for (var i = 0; i < distanceHits.Length; ++i)
                    {
                        AnyHits.Add(entity, distanceHits[i]);
                    }
                }
                distanceHits.Dispose();
            }
        }

        protected override void OnCreate()
        {
            var pointQueryDesc = new EntityQueryDesc
            {
                All = new ComponentType[]
                {
                    ComponentType.ReadOnly<ProximitySensor>(),
                    ComponentType.ReadOnly<Translation>(),
                    ComponentType.ReadOnly<Range>(),
                    // ComponentType.ReadOnly<LayerMask>()
                }
            };
            _pointQuery = GetEntityQuery(pointQueryDesc);

            var colliderQueryDesc = new EntityQueryDesc
            {
                All = new ComponentType[]
                {
                    ComponentType.ReadOnly<PhysicsCollider>(),
                    ComponentType.ReadOnly<ProximitySensor>(),
                    ComponentType.ReadOnly<Translation>(),
                    ComponentType.ReadOnly<Range>(),
                    // ComponentType.ReadOnly<LayerMask>()
                }
            };
            _colliderQuery = GetEntityQuery(colliderQueryDesc);
        }

        protected override JobHandle OnUpdate(JobHandle inputDependencies)
        {
            var pointJob = new PointJob
            {
                EntityType = GetEntityTypeHandle(),
                SensorType = GetComponentTypeHandle<ProximitySensor>(true),
                TranslationType = GetComponentTypeHandle<Translation>(true),
                RangeType = GetComponentTypeHandle<Range>(true),
                // LayerMaskType = GetArchetypeChunkComponentType<LayerMask>(true)
            };

            var colliderJob = new ColliderJob
            {
                EntityType = GetEntityTypeHandle(),
                SensorType = GetComponentTypeHandle<ProximitySensor>(true),
                TranslationType = GetComponentTypeHandle<Translation>(true),
                RangeType = GetComponentTypeHandle<Range>(true),
                // LayerMaskType = GetArchetypeChunkComponentType<LayerMask>(true)
            };

            var pointJobHandle = pointJob.Schedule(_pointQuery, inputDependencies);
            var colliderJobHandle = colliderJob.Schedule(_colliderQuery, inputDependencies);
            var jobHandle = JobHandle.CombineDependencies(pointJobHandle, colliderJobHandle);
            return jobHandle;
        }
    }
}
