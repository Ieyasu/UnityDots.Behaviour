using Unity.Entities;
using Unity.Mathematics;

namespace UnityDots.Behaviour.Steering
{
    [UpdateInGroup(typeof(SteeringSystemGroup))]
    public sealed class SteeringSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .ForEach((ref Acceleration a) => a.Value = float3.zero)
                .ScheduleParallel();
        }
    }
}
