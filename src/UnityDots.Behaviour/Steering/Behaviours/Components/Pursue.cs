using Unity.Entities;

namespace UnityDots.Behaviour.Steering
{
    public struct Pursue : IComponentData, ISteeringBehaviour
    {
        public float Weight { get; set; }
    }
}
