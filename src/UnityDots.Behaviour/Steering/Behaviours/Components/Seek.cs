using Unity.Entities;

namespace UnityDots.Behaviour.Steering
{
    public struct Seek : IComponentData, ISteeringBehaviour
    {
        public float Weight { get; set; }
    }
}
