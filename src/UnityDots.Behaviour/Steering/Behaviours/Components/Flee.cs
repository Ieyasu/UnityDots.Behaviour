using Unity.Entities;

namespace UnityDots.Behaviour.Steering
{
    public struct Flee : IComponentData, ISteeringBehaviour
    {
        public float Weight { get; set; }
    }
}
