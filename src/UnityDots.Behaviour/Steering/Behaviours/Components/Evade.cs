using Unity.Entities;

namespace UnityDots.Behaviour.Steering
{
    public struct Evade : IComponentData, ISteeringBehaviour
    {
        public float Weight { get; set; }
    }
}
