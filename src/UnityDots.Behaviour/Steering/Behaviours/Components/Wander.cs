using Unity.Entities;
using Unity.Mathematics;

namespace UnityDots.Behaviour.Steering
{
    public struct Wander : IComponentData, ISteeringBehaviour
    {
        public float Weight { get; set; }
        public float Strength;
        public float Frequency;
        public float3 Up;
    }
}
