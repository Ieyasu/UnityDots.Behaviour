using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityDots.Mathematics;
using UnityDots.Behaviour.Steering;
using UnityEngine;

[assembly: RegisterGenericJobType(typeof(PredictiveSteeringJob<Evade, EvadeSystem.DirectionProvider>))]
[assembly: RegisterGenericJobType(typeof(PredictiveSteeringJob<Pursue, PursueSystem.DirectionProvider>))]

namespace UnityDots.Behaviour.Steering
{
    public sealed class EvadeSystem : PredictiveSteeringSystem<Evade, EvadeSystem.DirectionProvider>
    {
        public struct DirectionProvider : IDirectionProvider
        {
            public float3 GetDirection(float3 position, float3 targetPosition)
            {
                return math.normalizesafe(position - targetPosition);
            }
        }
    }

    public sealed class PursueSystem : PredictiveSteeringSystem<Pursue, PursueSystem.DirectionProvider>
    {
        public struct DirectionProvider : IDirectionProvider
        {
            public float3 GetDirection(float3 position, float3 targetPosition)
            {
                return math.normalizesafe(targetPosition - position);
            }
        }
    }

    [BurstCompile]
    internal struct PredictiveSteeringJob<TBehaviour, TDirectionProvider> : IJobChunk
            where TBehaviour : struct, IComponentData, ISteeringBehaviour
            where TDirectionProvider : struct, IDirectionProvider
    {
        public float DeltaTime;

        public ComponentTypeHandle<Acceleration> AccelerationType;

        [ReadOnly] public BufferTypeHandle<TargetEntity> TargetType;
        [ReadOnly] public ComponentTypeHandle<TBehaviour> SteeringType;
        [ReadOnly] public ComponentTypeHandle<Translation> TranslationType;
        [ReadOnly] public ComponentTypeHandle<Velocity> VelocityType;
        [ReadOnly] public ComponentTypeHandle<VelocityLimit> VelocityLimitType;

        [ReadOnly] public ComponentDataFromEntity<Translation> TranslationFromEntity;
        [ReadOnly] public ComponentDataFromEntity<Velocity> VelocityFromEntity;

        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
        {
            var accelerations = chunk.GetNativeArray(AccelerationType);
            var targetsAccessor = chunk.GetBufferAccessor(TargetType);
            var steeringBehaviours = chunk.GetNativeArray(SteeringType);
            var translations = chunk.GetNativeArray(TranslationType);
            var velocities = chunk.GetNativeArray(VelocityType);
            var velocityLimits = chunk.GetNativeArray(VelocityLimitType);
            var directionProvider = new TDirectionProvider();

            for (var i = 0; i < chunk.Count; ++i)
            {
                var targets = targetsAccessor[i];
                var steeringBehaviour = steeringBehaviours[i];
                var position = translations[i].Value;
                var velocity = velocities[i].Value;
                var maxSpeed = velocityLimits[i].MaxValue;
                var acceleration = accelerations[i].Value;

                for (var j = 0; j < targets.Length; ++j)
                {
                    var target = targets[j];
                    if (!TranslationFromEntity.HasComponent(target) || !VelocityFromEntity.HasComponent(target))
                    {
                        continue;
                    }

                    var targetPosition = TranslationFromEntity[target].Value;
                    var targetVelocity = VelocityFromEntity[target].Value;

                    var speed = math.length(velocity);
                    var predictedPosition = Prediction.PredictIntersectPosition(targetPosition, targetVelocity, position, speed);
                    var direction = directionProvider.GetDirection(position, predictedPosition);
                    var desiredVelocity = direction * math.max(maxSpeed, 0);
                    var forceDelta = desiredVelocity - velocity;
                    acceleration += forceDelta * steeringBehaviour.Weight;
                }

                accelerations[i] = new Acceleration { Value = acceleration };
            }
        }
    }

    [UpdateInGroup(typeof(SteeringSystemGroup))]
    [UpdateAfter(typeof(SteeringSystem))]
    public abstract class PredictiveSteeringSystem<TBehaviour, TDirectionProvider
    > : JobComponentSystem
            where TBehaviour : struct, IComponentData, ISteeringBehaviour
            where TDirectionProvider : struct, IDirectionProvider
    {
        private EntityQuery _query;

        protected override void OnCreate()
        {
            var queryDesc = new EntityQueryDesc
            {
                All = new ComponentType[]
                {
                    typeof(Acceleration),
                    ComponentType.ReadOnly<TargetEntity>(),
                    ComponentType.ReadOnly<TBehaviour>(),
                    ComponentType.ReadOnly<Translation>(),
                    ComponentType.ReadOnly<Velocity>(),
                    ComponentType.ReadOnly<VelocityLimit>()
                }
            };
            _query = GetEntityQuery(queryDesc);
        }

        protected override JobHandle OnUpdate(JobHandle inputDependencies)
        {
            var job = new PredictiveSteeringJob<TBehaviour, TDirectionProvider>
            {
                DeltaTime = Time.DeltaTime,

                AccelerationType = GetComponentTypeHandle<Acceleration>(),
                TargetType = GetBufferTypeHandle<TargetEntity>(true),
                SteeringType = GetComponentTypeHandle<TBehaviour>(true),
                TranslationType = GetComponentTypeHandle<Translation>(true),
                VelocityType = GetComponentTypeHandle<Velocity>(true),
                VelocityLimitType = GetComponentTypeHandle<VelocityLimit>(true),

                TranslationFromEntity = GetComponentDataFromEntity<Translation>(true),
                VelocityFromEntity = GetComponentDataFromEntity<Velocity>(true),
            };

            return job.Schedule(_query, inputDependencies);
        }
    }
}
