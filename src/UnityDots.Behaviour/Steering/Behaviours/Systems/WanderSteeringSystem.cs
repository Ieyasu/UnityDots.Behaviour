using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

namespace UnityDots.Behaviour.Steering
{
    [UpdateInGroup(typeof(SteeringSystemGroup))]
    [UpdateAfter(typeof(SteeringSystem))]
    public sealed class WanderSteeringSystem : JobComponentSystem
    {
        private EntityQuery _query;

        [BurstCompile]
        private struct WanderSteeringJob : IJobChunk
        {
            public float Time;

            public ComponentTypeHandle<Acceleration> AccelerationType;
            public ComponentTypeHandle<Wander> SteeringType;

            [ReadOnly] public EntityTypeHandle EntityType;
            [ReadOnly] public ComponentTypeHandle<Velocity> VelocityType;
            [ReadOnly] public ComponentTypeHandle<VelocityLimit> VelocityLimitType;

            public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
            {
                var entities = chunk.GetNativeArray(EntityType);
                var accelerations = chunk.GetNativeArray(AccelerationType);
                var steeringBehaviours = chunk.GetNativeArray(SteeringType);
                var velocities = chunk.GetNativeArray(VelocityType);
                var velocityLimits = chunk.GetNativeArray(VelocityLimitType);

                for (var i = 0; i < chunk.Count; ++i)
                {
                    var entity = entities[i];
                    var steeringBehaviour = steeringBehaviours[i];
                    var velocity = velocities[i].Value;
                    var maxSpeed = velocityLimits[i].MaxValue;
                    var acceleration = accelerations[i];

                    // Use 2d perlin noise here so that the potentially large hash won't screw with floating point precision.
                    var hash = entity.Index;
                    var noise1 = noise.snoise(new float2(Time * steeringBehaviour.Frequency, hash));
                    var noise2 = noise.snoise(new float2(-Time * steeringBehaviour.Frequency, hash));
                    var wanderDirection = steeringBehaviour.Strength * math.normalizesafe(new float3(noise1, noise2, 0));
                    var up = steeringBehaviour.Up.Equals(float3.zero) ? new float3(0, 1, 0) : steeringBehaviour.Up;
                    var rotation = quaternion.LookRotationSafe(velocity, up);
                    var direction = math.mul(rotation, math.normalizesafe(new float3(0, 0, 1) + wanderDirection));

                    var desiredVelocity = direction * math.max(maxSpeed, 0);
                    var forceDelta = desiredVelocity - velocity;
                    acceleration.Value += forceDelta * steeringBehaviour.Weight;
                    steeringBehaviour.Up = math.mul(rotation, new float3(0, 1, 0));

                    steeringBehaviours[i] = steeringBehaviour;
                    accelerations[i] = acceleration;
                }
            }
        }

        protected override void OnCreate()
        {
            var queryDesc = new EntityQueryDesc
            {
                All = new ComponentType[]
                {
                    typeof(Acceleration),
                    typeof(Wander),
                    ComponentType.ReadOnly<Velocity>(),
                    ComponentType.ReadOnly<VelocityLimit>()
                }
            };
            _query = GetEntityQuery(queryDesc);
        }

        protected override JobHandle OnUpdate(JobHandle inputDependencies)
        {
            var job = new WanderSteeringJob
            {
                Time = (float)Time.ElapsedTime,
                EntityType = GetEntityTypeHandle(),
                AccelerationType = GetComponentTypeHandle<Acceleration>(),
                SteeringType = GetComponentTypeHandle<Wander>(),
                VelocityType = GetComponentTypeHandle<Velocity>(true),
                VelocityLimitType = GetComponentTypeHandle<VelocityLimit>(true),
            };

            return job.Schedule(_query, inputDependencies);
        }
    }
}
