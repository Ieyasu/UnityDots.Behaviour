using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityDots.Behaviour.Steering;

[assembly: RegisterGenericJobType(typeof(DirectedSteeringJob<Flee, FleeSystem.DirectionProvider>))]
[assembly: RegisterGenericJobType(typeof(DirectedSteeringJob<Seek, SeekSystem.DirectionProvider>))]

namespace UnityDots.Behaviour.Steering
{
    public sealed class FleeSystem : DirectedSteeringSystem<Flee, FleeSystem.DirectionProvider>
    {
        public struct DirectionProvider : IDirectionProvider
        {
            public float3 GetDirection(float3 position, float3 targetPosition)
            {
                return math.normalizesafe(position - targetPosition);
            }
        }
    }

    public sealed class SeekSystem : DirectedSteeringSystem<Seek, SeekSystem.DirectionProvider>
    {
        public struct DirectionProvider : IDirectionProvider
        {
            public float3 GetDirection(float3 position, float3 targetPosition)
            {
                return math.normalizesafe(targetPosition - position);
            }
        }
    }

    [BurstCompile]
    internal struct DirectedSteeringJob<TBehaviour, TDirectionProvider> : IJobChunk
            where TBehaviour : struct, IComponentData, ISteeringBehaviour
            where TDirectionProvider : struct, IDirectionProvider
    {
        public ComponentTypeHandle<Acceleration> AccelerationType;

        [ReadOnly] public BufferTypeHandle<TargetEntity> TargetType;
        [ReadOnly] public ComponentTypeHandle<TBehaviour> SteeringType;
        [ReadOnly] public ComponentTypeHandle<Translation> TranslationType;
        [ReadOnly] public ComponentTypeHandle<Velocity> VelocityType;
        [ReadOnly] public ComponentTypeHandle<VelocityLimit> VelocityLimitType;

        [ReadOnly] public ComponentDataFromEntity<Translation> TranslationFromEntity;

        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
        {
            var accelerations = chunk.GetNativeArray(AccelerationType);
            var targetsAccessor = chunk.GetBufferAccessor(TargetType);
            var steeringBehaviours = chunk.GetNativeArray(SteeringType);
            var translations = chunk.GetNativeArray(TranslationType);
            var velocities = chunk.GetNativeArray(VelocityType);
            var velocityLimits = chunk.GetNativeArray(VelocityLimitType);
            var directionProvider = new TDirectionProvider();

            for (var i = 0; i < chunk.Count; ++i)
            {
                var targets = targetsAccessor[i];
                var steeringBehaviour = steeringBehaviours[i];
                var position = translations[i].Value;
                var velocity = velocities[i].Value;
                var maxSpeed = velocityLimits[i].MaxValue;
                var acceleration = accelerations[i].Value;

                for (var j = 0; j < targets.Length; ++j)
                {
                    var target = targets[j];
                    if (!TranslationFromEntity.HasComponent(target))
                    {
                        continue;
                    }

                    var targetPosition = TranslationFromEntity[target].Value;
                    var direction = directionProvider.GetDirection(position, targetPosition);
                    var desiredVelocity = direction * math.max(maxSpeed, 0);
                    var forceDelta = desiredVelocity - velocity;
                    acceleration += forceDelta * steeringBehaviour.Weight;
                }

                accelerations[i] = new Acceleration { Value = acceleration };
            }
        }
    }

    [UpdateInGroup(typeof(SteeringSystemGroup))]
    [UpdateAfter(typeof(SteeringSystem))]
    public abstract class DirectedSteeringSystem<TBehaviour, TDirectionProvider> : JobComponentSystem
            where TBehaviour : struct, IComponentData, ISteeringBehaviour
            where TDirectionProvider : struct, IDirectionProvider
    {
        private EntityQuery _query;

        protected override void OnCreate()
        {
            var queryDesc = new EntityQueryDesc
            {
                All = new ComponentType[]
                {
                    typeof(Acceleration),
                    ComponentType.ReadOnly<TargetEntity>(),
                    ComponentType.ReadOnly<TBehaviour>(),
                    ComponentType.ReadOnly<Translation>(),
                    ComponentType.ReadOnly<Velocity>(),
                    ComponentType.ReadOnly<VelocityLimit>()
                }
            };
            _query = GetEntityQuery(queryDesc);
        }

        protected override JobHandle OnUpdate(JobHandle inputDependencies)
        {
            var job = new DirectedSteeringJob<TBehaviour, TDirectionProvider>
            {
                AccelerationType = GetComponentTypeHandle<Acceleration>(),
                TargetType = GetBufferTypeHandle<TargetEntity>(true),
                SteeringType = GetComponentTypeHandle<TBehaviour>(true),
                TranslationType = GetComponentTypeHandle<Translation>(true),
                VelocityType = GetComponentTypeHandle<Velocity>(true),
                VelocityLimitType = GetComponentTypeHandle<VelocityLimit>(true),
                TranslationFromEntity = GetComponentDataFromEntity<Translation>(true),
            };

            return job.Schedule(_query, inputDependencies);
        }
    }
}
