using Unity.Entities;

namespace UnityDots.Behaviour.Steering
{
    [InternalBufferCapacity(3)]
    public struct SteeringGroup : IBufferElementData
    {
        public Entity Group;
        public float Weight;
    }
}
