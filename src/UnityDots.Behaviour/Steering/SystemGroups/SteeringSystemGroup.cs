using Unity.Entities;

namespace UnityDots.Behaviour.Steering
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateBefore(typeof(MovementSystemGroup))]
    public sealed class SteeringSystemGroup : ComponentSystemGroup
    {
    }
}
