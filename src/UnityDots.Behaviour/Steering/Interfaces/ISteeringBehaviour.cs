namespace UnityDots.Behaviour.Steering
{
    public interface ISteeringBehaviour
    {
        float Weight { get; }
    }
}
