using Unity.Mathematics;

namespace UnityDots.Behaviour.Steering
{
    public interface IDirectionProvider
    {
        float3 GetDirection(float3 position, float3 targetPosition);
    }
}
